<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateQuestionAndAnswerCollection extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:create-question-answer-collection';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->indexForAnswersCollection();
        $this->indexForQuestionsCollection();
    }

    protected function indexForQuestionsCollection()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->index('group.group_id', 'questions_index_group_id');
            $table->index('status', 'questions_index_status');
            $table->index('user_id', 'questions_index_user_id');
            $table->index('field_id', 'questions_index_field_id');
            $table->index('tags', 'questions_index_tags');
            $table->index('created_at', 'questions_index_created_at');
            $table->index('updated_at', 'questions_index_updated_at');
            $table->index('price', 'questions_index_price');
            $table->index('visibility', 'questions_visibility');
            $table->index(['user_id', '_id'], 'questions_index_user_id_id');
            $table->index(['user_id', 'status'], 'questions_index_user_id_status');
            $table->index(['user_id', 'field_id'], 'questions_index_user_id_field_id');
            $table->index(['user_id', 'price'], 'questions_index_user_id_value');
            $table->index(['user_id', 'visibility'], 'questions_index_user_id_visibility');
        });
    }

    protected function indexForAnswersCollection()
    {
        Schema::table('answers', function (Blueprint $table) {
            $table->index('users_id', 'answers_index_users_id');
            $table->index('questions_id', 'answers_index_questions_id');
            $table->index('status', 'answers_index_status');
            $table->index('created_at', 'answers_index_created_at');
            $table->index(['users_id', 'status'], 'answers_index_users_id_accepted');
            $table->index(['users_id', 'questions_id'], 'answers_index_users_id_questions_id');
        });
    }
}
