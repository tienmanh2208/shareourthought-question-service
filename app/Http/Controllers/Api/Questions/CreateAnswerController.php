<?php

namespace App\Http\Controllers\Api\Questions;

use App\Enums\AnswerStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Question;
use Illuminate\Support\Facades\Validator;

class CreateAnswerController extends Controller
{
    protected $question;
    protected $answer;

    public function __construct(Question $question, Answer $answer)
    {
        $this->question = $question;
        $this->answer = $answer;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $checkValidation->errors()->first(),
            ]);
        }

        if (!$this->checkPermission($params['user_id'], $params['question_id'])) {
            return response()->json([
                'code' => 403,
                'message' => trans('responses.request_denied'),
            ]);
        };

        $this->createAnswer($params);

        return response()->json([
            'code' => 201,
            'message' => trans('responses.create_answer_successfully'),
        ]);
    }

    /**
     * Get params from request
     */
    protected function getParams(Request $request)
    {
        return $request->only([
            'user_id',
            'question_id',
            'content',
        ]);
    }

    /**
     * Define rules for validation
     */
    protected function rules()
    {
        return [
            'user_id' => 'required|int',
            'question_id' => 'required|string',
            'content' => 'required|string',
        ];
    }

    /**
     * Check permission when create answer
     */
    protected function checkPermission(int $userId, string $questionId)
    {
        if (!$this->question->checkExistanceOfQuestion($questionId)) {
            return false;
        };

        $questionInfo = $this->question->getQuestionInfo($questionId);

        if ($questionInfo->user_id == $userId) {
            return false;
        }

        return true;
    }

    /**
     * Create answer for question
     */
    public function createAnswer($params)
    {
        $this->answer->createAnswer($params);
    }
}
