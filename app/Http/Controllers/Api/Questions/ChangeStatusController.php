<?php

namespace App\Http\Controllers\Api\Questions;

use App\Models\Question;
use Illuminate\Http\Request;
use App\Enums\QuestionStatus;
use App\Services\QuestionService;
use BenSampo\Enum\Rules\EnumValue;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ChangeStatusController extends Controller
{
    protected $questionService;
    protected $question;

    public function __construct(QuestionService $questionService, Question $question)
    {
        $this->questionService = $questionService;
        $this->question = $question;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $checkValidation->errors()->first()
            ]);
        }

        if (!$this->checkPermission($params['user_id'], $params['question_id'], $params['status'])) {
            return response()->json([
                'code' => 400,
                'message' => trans('responses.request_denied'),
            ]);
        };

        $this->changeStatus($params['status'], $params['question_id']);
        return response()->json([
            'code' => 203,
            'message' => trans('responses.update_question_status_successfully'),
        ]);
    }

    /**
     * Get params from request
     */
    protected function getParams(Request $request)
    {
        return $request->only(['user_id', 'question_id', 'status']);
    }

    /**
     * Define rules for validation
     */
    protected function rules()
    {
        return [
            'user_id' => 'required|int',
            'question_id' => 'required|string',
            'status' => ['required', new EnumValue(QuestionStatus::class)],
        ];
    }

    /**
     * Check permission when change status
     */
    protected function checkPermission(int $userId, string $questionId, int $questionStatus)
    {
        if ($questionStatus == QuestionStatus::CANCEL) {
            return $this->questionService->canChangeInfoOfQuestion($userId, $questionId);
        } else {
            return $this->questionService->isTheAuthorOfQuestion($userId, $questionId);
        }
    }

    /**
     * Change status of question
     */
    protected function changeStatus(int $status, string $questionId)
    {
        $this->question->changeStatus($status, $questionId);
    }
}
