<?php

namespace App\Http\Controllers\Api\Questions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Support\Facades\Validator;

class CreateQuestionController extends Controller
{
    protected $question;

    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);
        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response([
                'code' => 400,
                'message' => $checkValidation->errors()->first(),
            ]);
        }

        $response = $this->createQuestion($params);
        return response([
            'code' => 201,
            'message' => trans('responses.create_question_successfully'),
            'data' => [
                'question_id' => $response->_id,
            ]
        ]);
    }

    protected function getParams(Request $request)
    {
        return $request->only([
            'user_id',
            'question_content',
            'price',
            'visibility',
            'field_id',
            'group',
            'tags',
        ]);
    }

    protected function rules()
    {
        return [
            'user_id' => 'required|int',
            'question_content' => 'required|array',
            'content.*' => 'required|string',
            'price' => 'required|int',
            'visibility' => 'required|int',
            'field_id' => 'required|int',
            'group' => 'array|nullable',
            'group.*' => 'int|nullable',
            'tags' => 'required|array',
        ];
    }

    /**
     * Call model to create question
     * 
     * @param array $params [
     *  'user_id',
     *  'question_content',
     *  'price',
     *  'visibility',
     *  'field_id',
     *  'group',
     *  'tags',
     * ]
     */
    protected function createQuestion(array $params)
    {
        return $this->question->createNewQuestion($params);
    }
}
