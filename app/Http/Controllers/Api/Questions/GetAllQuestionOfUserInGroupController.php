<?php

namespace App\Http\Controllers\Api\Questions;

use App\Models\Question;
use App\Http\Controllers\Controller;

class GetAllQuestionOfUserInGroupController extends Controller
{
    protected $question;

    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    public function main(int $groupId, int $userId)
    {
        $returnData = $this->question->getAllQuestionOfUserInGroup($groupId, $userId);

        return response()->json([
            'code' => 200,
            'data' => $returnData
        ], 200);
    }
}
