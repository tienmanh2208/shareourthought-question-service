<?php

namespace App\Http\Controllers\Api\Questions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\QuestionService;

class GetAllFieldOfUserController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main(int $userId)
    {
        return response()->json([
            'code' => 200,
            'data' => $this->questionService->getAllFieldOfUser($userId)
        ]);
    }
}
