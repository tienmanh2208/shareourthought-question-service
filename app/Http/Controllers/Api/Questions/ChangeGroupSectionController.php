<?php

namespace App\Http\Controllers\Api\Questions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use Illuminate\Support\Facades\Validator;

class ChangeGroupSectionController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $checkValidation->errors()->first(),
            ]);
        }

        if (!$this->questionService->doesQuestionBelongToGroup($params['question_id'], $params['group_id'])) {
            return response()->json([
                'code' => 400,
                'message' => trans('responses.request_denied'),
            ]);
        };

        $this->questionService->updateGroupIdForQuestion($params['question_id'], $params['group_id'], $params['group_section_id']);

        return response()->json([
            'code' => 203,
            'message' => trans('responses.update_question_section_id_successfully'),
        ]);
    }

    protected function getParams(Request $request)
    {
        return $request->only(['user_id', 'question_id', 'group_id', 'group_section_id']);
    }

    protected function rules()
    {
        return [
            'user_id' => 'required|int',
            'question_id' => 'required|string',
            'group_id' => 'required|int',
            'group_section_id' => 'required|int',
        ];
    }
}
