<?php

namespace App\Http\Controllers\Api\Questions;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FindQuestionController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $checkValidation->errors()->first()
            ]);
        }

        $listQuestion = $this->questionService->findQuestion(
            $params['title'],
            $params['content'],
            $params['tags'],
            $params['price_from'],
            $params['price_to'],
            $params['user_id'],
            $params['field_id']
        );

        return response()->json([
            'code' => 200,
            'data' => $listQuestion
        ], 200);
    }

    protected function getParams(Request $request)
    {
        return $request->only(['title', 'content', 'tags', 'price_from', 'price_to', 'user_id', 'field_id']);
    }

    protected function rules()
    {
        return [
            'title' => 'string|nullable',
            'content' => 'string|nullable',
            'tags' => 'array|nullable',
            'price_from' => 'integer|nullable',
            'price_to' => 'integer|nullable',
            'user_id' => 'integer|nullable',
            'field_id' => 'integer|nullable'
        ];
    }
}
