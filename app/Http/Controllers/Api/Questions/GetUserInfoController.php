<?php

namespace App\Http\Controllers\Api\Questions;

use App\Http\Controllers\Controller;
use App\Services\AnswerService;
use App\Services\QuestionService;

class GetUserInfoController extends Controller
{
    protected $questionService;
    protected $answerService;

    public function __construct(QuestionService $questionService, AnswerService $answerService)
    {
        $this->questionService = $questionService;
        $this->answerService = $answerService;
    }

    public function main($userId)
    {
        return response()->json([
            'code' => 200,
            'data' => [
                'quetion_info' => $this->questionService->getSummaryQuestionOfUser($userId),
                'answer_info' => $this->answerService->getSummarayAnswerForUser($userId),
            ]
        ], 200);
    }
}
