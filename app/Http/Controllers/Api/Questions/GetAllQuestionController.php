<?php

namespace App\Http\Controllers\Api\Questions;

use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GetAllQuestionController extends Controller
{
    protected $question;

    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    public function main(Request $request)
    {
        return response()->json([
            'code' => 200,
            'data' => $this->getAllQuestions($request->field_id),
        ]);
    }

    /**
     * Get all question
     */
    public function getAllQuestions($fieldId)
    {
        if (is_null($fieldId)) {
            return $this->question->getAllQuestion();
        } else {
            return $this->question->getAllQuestionByFieldId($fieldId);
        }
    }
}
