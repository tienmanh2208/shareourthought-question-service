<?php

namespace App\Http\Controllers\Api\Questions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use Illuminate\Support\Facades\Validator;

class ChangeContentOfQuestionController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $checkValidation->errors()->first(),
            ]);
        }

        if (!$this->checkPermission($params['user_id'], $params['question_id'])) {
            return response()->json([
                'code' => 400,
                'message' => trans('responses.request_denied'),
            ]);
        };

        $this->questionService->updateContentQuestion($params['question_id'], $params['content']);

        return response()->json([
            'code' => 203,
            'message' => trans('responses.update_question_content_successfully'),
        ]);
    }

    protected function getParams(Request $request)
    {
        return $request->only(['user_id', 'question_id', 'content']);
    }

    protected function rules()
    {
        return [
            'user_id' => 'required|int',
            'question_id' => 'required|string',
            'content' => 'required|array',
            'content.title' => 'required|string',
            'content.content' => 'required|string',
        ];
    }

    protected function checkPermission(int $userId, string $questionId)
    {
        if (
            $this->questionService->isTheAuthorOfQuestion($userId, $questionId)
            && !$this->questionService->hasAnyAnswer($questionId)
        ) {
            return true;
        }

        return false;
    }
}
