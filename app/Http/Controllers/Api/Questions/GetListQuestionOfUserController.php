<?php

namespace App\Http\Controllers\Api\Questions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Question;

class GetListQuestionOfUserController extends Controller
{
    protected $question;

    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    public function main(int $userId, Request $request)
    {
        return response()->json([
            'code' => 200,
            'data' => $this->getListQuestion($userId, $request->field_id, $request->per_page),
        ]);
    }

    /**
     * Get list question
     */
    public function getListQuestion(int $userId, $fieldId, $perPage)
    {
        if (is_null($fieldId)) {
            return $this->question->getAllQuestionOfUser($userId, 'created_at', 'DESC', is_null($perPage) ? 4 : $perPage);
        } else {
            return $this->question->getListQuestionOfUserByFieldId($userId, $fieldId, 'created_at', 'DESC', is_null($perPage) ? 4 : $perPage);
        }
    }
}
