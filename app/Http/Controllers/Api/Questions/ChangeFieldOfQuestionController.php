<?php

namespace App\Http\Controllers\Api\Questions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Services\QuestionService;
use Illuminate\Support\Facades\Validator;

class ChangeFieldOfQuestionController extends Controller
{
    protected $questionService;
    protected $question;

    public function __construct(QuestionService $questionService, Question $question)
    {
        $this->questionService = $questionService;
        $this->question = $question;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $checkValidation->errors()->first(),
            ]);
        }

        if (!$this->questionService->isTheAuthorOfQuestion($params['user_id'], $params['question_id'])) {
            return response()->json([
                'code' => 400,
                'message' => trans('responses.request_denied'),
            ]);
        }

        $this->question->changeField($params['field_id'], $params['question_id']);

        return response()->json([
            'code' => 203,
            'message' => trans('responses.update_question_field_successfully'),
        ]);
    }

    protected function getParams(Request $request)
    {
        return $request->only(['user_id', 'question_id', 'field_id']);
    }

    protected function rules()
    {
        return [
            'user_id' => 'required|int',
            'question_id' => 'required|string',
            'field_id' => 'required|int',
        ];
    }
}
