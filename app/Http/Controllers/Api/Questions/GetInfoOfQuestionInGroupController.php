<?php

namespace App\Http\Controllers\Api\Questions;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;

class GetInfoOfQuestionInGroupController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main($groupId)
    {
        return response()->json([
            'code' => 200,
            'data' => $this->questionService->getDetailOfGroup($groupId),
        ], 200);
    }
}
