<?php

namespace App\Http\Controllers\Api\Questions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Services\QuestionService;
use Illuminate\Support\Facades\Validator;

class ChangePriceOfQuestion extends Controller
{
    protected $questionService;
    protected $question;

    public function __construct(QuestionService $questionService, Question $question)
    {
        $this->questionService = $questionService;
        $this->question = $question;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $checkValidation->errors()->first(),
            ]);
        }

        if (!$this->questionService->canChangePriceOfQuestion($params['user_id'], $params['question_id'])) {
            return response()->json([
                'code' => 400,
                'message' => trans('responses.request_denied'),
            ]);
        };

        $this->updatePrice($params['price'], $params['question_id']);

        return response()->json([
            'code' => 203,
            'message' => trans('responses.update_question_price_successfully'),
        ]);
    }

    protected function getParams(Request $request)
    {
        return $request->only(['user_id', 'question_id', 'price']);
    }

    protected function rules()
    {
        return [
            'user_id' => 'required|int',
            'question_id' => 'required|string',
            'price' => 'required|int',
        ];
    }

    protected function updatePrice(int $price, string $questionId)
    {
        $this->question->updatePrice($price, $questionId);
    }
}
