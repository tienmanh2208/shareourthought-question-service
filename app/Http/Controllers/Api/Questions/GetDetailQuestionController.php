<?php

namespace App\Http\Controllers\Api\Questions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use Illuminate\Support\Facades\Validator;

class GetDetailQuestionController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main($questionId, Request $request)
    {
        $params = $this->getParams($request);

        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $checkValidation->errors()->first()
            ]);
        }

        $response = $this->questionService->getDetailOfQuestion($questionId, $params['user_id']);

        if (is_null($response)) {
            return response()->json([
                'code' => 403,
                'message' => trans('responses.have_no_permission'),
            ], 200);
        }

        return response()->json([
            'code' => 200,
            'data' => $response,
        ], 200);
    }

    /**
     * Get params from request
     */
    protected function getParams(Request $request)
    {
        return $request->only(['user_id']);
    }

    /**
     * Define rules for controller
     */
    protected function rules()
    {
        return [
            'user_id' => 'required|int'
        ];
    }
}
