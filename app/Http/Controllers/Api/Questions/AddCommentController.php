<?php

namespace App\Http\Controllers\Api\Questions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Support\Facades\Validator;

class AddCommentController extends Controller
{
    protected $question;

    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $checkValidation->errors()->first(),
            ]);
        }

        $this->question->addComment($params['commentator_id'], $params['question_id'], $params['content']);

        return response()->json([
            'code' => 203,
            'message' => trans('responses.add_comment_successfully'),
        ]);
    }

    protected function getParams(Request $request)
    {
        return $request->only(['commentator_id', 'question_id', 'content']);
    }

    protected function rules()
    {
        return [
            'commentator_id' => 'required|int',
            'question_id' => 'required|string',
            'content' => 'required|string',
        ];
    }
}
