<?php

namespace App\Http\Controllers\Api\Questions;

use App\Enums\QuestionVisibility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Services\QuestionService;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Facades\Validator;

class ChangeVisibilityController extends Controller
{
    protected $questionService;
    protected $question;

    public function __construct(QuestionService $questionService, Question $question)
    {
        $this->questionService = $questionService;
        $this->question = $question;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $checkValidation->errors()->first()
            ]);
        }

        if (!$this->questionService->canChangeInfoOfQuestion($params['user_id'], $params['question_id'])) {
            return response()->json([
                'code' => 400,
                'message' => trans('responses.request_denied'),
            ]);
        };

        $this->changeVisibility($params['visibility'], $params['question_id']);

        return response()->json([
            'code' => 203,
            'message' => trans('responses.update_question_visibility_successfully'),
        ]);
    }


    protected function getParams(Request $request)
    {
        return $request->only(['user_id', 'question_id', 'visibility']);
    }

    protected function rules()
    {
        return [
            'user_id' => 'required|int',
            'question_id' => 'required|string',
            'visibility' => ['required', new EnumValue(QuestionVisibility::class)],
        ];
    }

    protected function changeVisibility(int $visibility, string $questionId)
    {
        $this->question->changeVisibility($visibility, $questionId);
    }
}
