<?php

namespace App\Http\Controllers\Api\Questions;

use App\Http\Controllers\Controller;
use App\Models\Question;

class GetListQuestionOfGroupController extends Controller
{
    protected $question;

    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    public function main(int $groupId, int $groupSectionId)
    {
        if ($groupSectionId == 0) {
            $returnData = $this->question->getAllQuestionOfGroup($groupId);
        } else {
            $returnData = $this->question->getAllQuestionOfGroupBySectionId($groupId, $groupSectionId);
        }

        return response()->json([
            'code' => 200,
            'data' => $returnData
        ], 200);
    }
}
