<?php

namespace App\Http\Controllers\Api\Answers;

use App\Http\Controllers\Controller;
use App\Services\AnswerService;
use Illuminate\Http\Request;

class UpdateAnswerStatusAdminController extends Controller
{
    protected $answerService;

    public function __construct(AnswerService $answerService)
    {
        $this->answerService = $answerService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);
        $userId = $this->answerService->updateStatusAnswer(
            $params['answer_id'],
            $params['status'],
            $params['question_id']
        );

        return response()->json([
            'code' => 203,
            'message' => trans('responses.update_answer_status_successfully'),
            'data' => [
                'user_id' => $userId
            ]
        ]);
    }

    protected function getParams(Request $request)
    {
        return $request->only(['answer_id', 'status', 'question_id']);
    }
}
