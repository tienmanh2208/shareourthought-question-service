<?php

namespace App\Http\Controllers\Api\Answers;

use App\Http\Controllers\Controller;
use App\Services\AnswerService;
use Illuminate\Http\Request;

class UpdateReportStatusController extends Controller
{
    protected $answerService;

    public function __construct(AnswerService $answerService)
    {
        $this->answerService = $answerService;
    }

    public function main(string $answerId, Request $request)
    {
        $this->answerService->updateReportStatusForAnswer($answerId, $request->status);

        return response()->json([
            'code' => 203,
            'message' => trans('responses.answer.update_report_status_successfully')
        ], 200);
    }
}
