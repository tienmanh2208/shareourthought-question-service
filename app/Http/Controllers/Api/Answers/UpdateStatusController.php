<?php

namespace App\Http\Controllers\Api\Answers;

use App\Enums\AnswerStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AnswerService;
use App\Services\QuestionService;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Support\Facades\Validator;

class UpdateStatusController extends Controller
{
    protected $questionService;
    protected $answerService;

    public function __construct(QuestionService $questionService, AnswerService $answerService)
    {
        $this->questionService = $questionService;
        $this->answerService = $answerService;
    }

    public function main(Request $request)
    {
        $params = $this->getParams($request);

        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $checkValidation->errors()->first(),
            ]);
        }

        if (!$this->checkPermission($params['user_id'], $params['answer_id'], $params['question_id'])) {
            return response()->json([
                'code' => 400,
                'message' => trans('responses.request_denied'),
            ]);
        };

        $userId = $this->answerService->updateStatusAnswer($params['answer_id'], $params['status'], $params['question_id']);

        return response()->json([
            'code' => 203,
            'message' => trans('responses.update_answer_status_successfully'),
            'data' => [
                'user_id' => $userId
            ]
        ]);
    }

    protected function getParams(Request $request)
    {
        return $request->only(['user_id', 'answer_id', 'status', 'question_id']);
    }

    protected function rules()
    {
        return [
            'user_id' => 'required|int',
            'answer_id' => 'required|string',
            'status' => ['required', new EnumValue(AnswerStatus::class)],
            'question_id' => 'required|string'
        ];
    }

    protected function checkPermission(int $userId, string $answerId, string $questionId)
    {
        if (!$this->questionService->isTheAuthorOfQuestion($userId, $questionId)) {
            return false;
        }

        return $this->answerService->doesAnswerBelongToQuestion($answerId, $questionId);
    }
}
