<?php

namespace App\Http\Controllers\Api\Answers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AnswerService;
use App\Services\QuestionService;
use Illuminate\Support\Facades\Validator;

class GetDetailOfAnswerController extends Controller
{
    protected $answerService;
    protected $questionService;

    public function __construct(AnswerService $answerService, QuestionService $questionService)
    {
        $this->answerService = $answerService;
        $this->questionService = $questionService;
    }

    public function main(string $answerId, Request $request)
    {
        $params = $this->getParams($request);

        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $checkValidation->errors()->first(),
            ]);
        }

        return response()->json($this->answerService->getAnswerDetail($params['user_id'], $answerId), 200);
    }

    /**
     * Get params from request
     * 
     * @param Request $request
     */
    protected function getParams(Request $request)
    {
        return $request->only(['user_id']);
    }

    /**
     * Define rules for controller
     */
    protected function rules()
    {
        return [
            'user_id' => 'required|int',
        ];
    }
}
