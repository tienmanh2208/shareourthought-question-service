<?php

namespace App\Http\Controllers\Api\Answers;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;
use Illuminate\Http\Request;

class GetListQuestionByListAnswerIdController extends Controller
{
    protected $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function main(Request $request)
    {
        return response()->json([
            'code' => 200,
            'data' => $this->questionService->getListQuestionByListAnswerId($request->list_question_id)
        ], 200);
    }
}
