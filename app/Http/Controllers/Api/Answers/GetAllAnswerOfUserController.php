<?php

namespace App\Http\Controllers\Api\Answers;

use App\Http\Controllers\Controller;
use App\Services\AnswerService;
use App\Services\QuestionService;

class GetAllAnswerOfUserController extends Controller
{
    protected $answerService;
    protected $questionService;

    public function __construct(AnswerService $answerService, QuestionService $questionService)
    {
        $this->answerService = $answerService;
        $this->questionService = $questionService;
    }

    public function main($userId)
    {
        $listAnswer = $this->answerService->getAllAnswerOfUser($userId);
        $listQuestionId = $this->getListQuestionId($listAnswer['data']);
        $listQuestionInfo = $this->questionService->getListQuestionByListQuestionId($listQuestionId);
        $listConvertedQuestion = [];

        foreach ($listQuestionInfo as $questionInfo) {
            $listConvertedQuestion[$questionInfo->_id] = $questionInfo;
        }

        foreach ($listAnswer['data'] as &$answer) {
            $answer['question_info'] = $listConvertedQuestion[$answer['question_id']];
        }

        return response()->json([
            'code' => 200,
            'data' => $listAnswer
        ], 200);
    }

    protected function getListQuestionId(array $listAnswer)
    {
        return array_map(function ($answer) {
            return $answer['question_id'];
        }, $listAnswer);
    }
}
