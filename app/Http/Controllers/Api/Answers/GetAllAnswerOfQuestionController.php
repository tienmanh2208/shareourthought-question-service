<?php

namespace App\Http\Controllers\Api\Answers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AnswerService;
use Illuminate\Support\Facades\Validator;

class GetAllAnswerOfQuestionController extends Controller
{
    protected $answerService;

    public function __construct(AnswerService $answerService)
    {
        $this->answerService = $answerService;
    }

    public function main($questionId, Request $request)
    {

        $params = $this->getParams($request);

        $checkValidation = Validator::make($params, $this->rules());

        if ($checkValidation->fails()) {
            return response()->json([
                'code' => 400,
                'message' => $checkValidation->errors()->first()
            ]);
        }

        return response()->json([
            'code' => 200,
            'data' => $this->answerService->getAllAnswerOfQuestion($questionId)
        ]);
    }

    /**
     * Get params from request
     */
    protected function getParams(Request $request)
    {
        return $request->only(['user_id']);
    }

    /**
     * Define rules for controller
     */
    protected function rules()
    {
        return [
            'user_id' => 'required|int'
        ];
    }
}
