<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class QuestionStatus extends Enum
{
    const OPEN = 1;
    const RESOLVED = 2;
    const CANCEL = 3;
    const DELETED = 4;
}
