<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class Role extends Enum
{
    const QUESTIONER = 1;
    const ANSWERER = 2;
    const VIEWER = 3;
}
