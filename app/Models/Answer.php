<?php

namespace App\Models;

use App\Enums\AnswerStatus;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use MongoDB\BSON\ObjectId;

class Answer extends Eloquent
{
    protected $fillable = [
        'user_id',
        'question_id',
        'content',
        'status',
        'report_status'
    ];

    /**
     * Create answer for question
     * 
     * @param array $params [
     *  user_id,
     *  question_id,
     *  content,
     * ]
     */
    public function createAnswer(array $params)
    {
        $this->create([
            'user_id' => (int) $params['user_id'],
            'question_id' => new ObjectId($params['question_id']),
            'content' => $params['content'],
            'status' => AnswerStatus::WAITING,
        ]);
    }

    /**
     * Get answer for question
     * 
     * @param string $questionId
     */
    public function getAnswersOfQuestion(string $questionId)
    {
        return $this->where('question_id', new ObjectId($questionId))
            ->get();
    }

    /**
     * Get answer by id
     */
    public function getAnswerById(string $answerId)
    {
        return $this->where('_id', new ObjectId($answerId))
            ->first();
    }

    /**
     * Update status for answer
     */
    public function updateStatus(string $answerId, int $status)
    {
        return $this->where('_id', new ObjectId($answerId))
            ->update(['status' => $status]);
    }

    /**
     * Get all answer of a question without content
     * 
     * @param string $questionId
     */
    public function getAllAnswerOfQuestion(string $questionId)
    {
        return $this->where('question_id', new ObjectId($questionId))
            ->get([
                '_id',
                'user_id',
                'question_id',
                'status',
                'created_at'
            ]);
    }

    /**
     * Check if questioner can view answer or not
     * Questioner can view answer only if there is no accepted or considering answer
     * 
     * @param string $questionId
     * @param string $answerId
     * 
     * @return boolean
     */
    public function canQuestionerView(string $questionId, string $answerId)
    {
        $answerInfo = $this->where('question_id', new ObjectId($questionId))
            ->where('_id', '!=', new ObjectId($answerId))
            ->whereIn('status', [AnswerStatus::CONSIDERING, AnswerStatus::ACCEPTED])
            ->first();

        if (is_null($answerInfo)) {
            return true;
        }

        return false;
    }

    /**
     * Check if viewer can view answer or not
     * User can view answer only if question  is public and the requested answer is accepted
     * 
     * @param string $answerId
     * 
     * @return boolean
     */
    public function canViewerView(string $answerId)
    {
        if ($this->getAnswerById($answerId)->status == AnswerStatus::ACCEPTED) {
            return true;
        }

        return false;
    }

    /**
     * Get summary answer for user
     * 
     * @param int $userId
     */
    public function getSummaryAnswerOfUser(int $userId)
    {
        return $this->raw(function ($collection) use ($userId) {
            return $collection->aggregate([
                (object) [
                    '$match' => (object) [
                        'user_id' => $userId
                    ]
                ],
                (object) [
                    '$group' => (object) [
                        '_id' => '$status',
                        'count' => (object) [
                            '$sum' => 1
                        ]
                    ]
                ]
            ]);
        });
    }

    /**
     * Update the status of report for answer
     * 
     * @param string $answerId
     * @param int $reportStatus
     */
    public function updateReportStatus(string $answerId, int $reportStatus)
    {
        return $this->where('_id', new ObjectId($answerId))
            ->update(['report_status' => $reportStatus]);;
    }

    /**
     * Get all answers of user
     * 
     * @param int $userId
     */
    public function getAllAnswerOfUser(int $userId, int $perPage = 5, string $orderBy = 'created_at', string $order = 'DESC')
    {
        return $this->where('user_id', $userId)
            ->orderBy($orderBy, $order)
            ->paginate($perPage);
    }
}
