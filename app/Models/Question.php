<?php

namespace App\Models;

use App\Enums\QuestionStatus;
use App\Enums\QuestionVisibility;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;

class Question extends Eloquent
{
    protected $collection = 'questions';
    protected $fillable = [
        'user_id',
        'question_content',
        'price',
        'visibility',
        'status',
        'upvotes',
        'downvotes',
        'field_id',
        'group',
        'tags',
        'comments'
    ];

    /**
     * Create new question
     * 
     * @param array $params [
     *  'user_id',
     *  'question_content',
     *  'price',
     *  'visibility',
     *  'field_id',
     *  'group',
     *  'tags',
     * ]
     */
    public function createNewQuestion(array $params)
    {
        return $this->create([
            'user_id' => $params['user_id'],
            'price' => $params['price'],
            'visibility' => $params['visibility'],
            'status' => QuestionStatus::OPEN,
            'upvotes' => 0,
            'downvotes' => 0,
            'field_id' => $params['field_id'],
            'group' => $params['group'],
            'tags' => $params['tags'],
            'comments' => [],
            'question_content' => $params['question_content'],
        ]);
    }

    /**
     * Get basic info of question
     */
    public function getQuestionInfo(string $questionId)
    {
        return $this->where('_id', new ObjectId($questionId))->first();
    }

    /**
     * Check if question exists or not
     */
    public function checkExistanceOfQuestion(string $questionId)
    {
        if (is_null($this->getQuestionInfo($questionId))) {
            return false;
        }

        return true;
    }

    /**
     * Get list questions of an user by field id
     */
    public function getListQuestionOfUserByFieldId(int $userId, int $fieldId, string $orderBy = 'created_at', string $order = 'DESC', int $perPage = 4)
    {
        return $this->where('user_id', $userId)
            ->where('field_id', $fieldId)
            ->where('group.group_id', null)
            ->orderBy($orderBy, $order)
            ->paginate($perPage);
    }

    /**
     * Get all question of user
     */
    public function getAllQuestionOfUser(int $userId, string $orderBy = 'created_at', string $order = 'DESC', int $perPage = 4)
    {
        return $this->where('user_id', $userId)
            ->where('group.group_id', null)
            ->orderBy($orderBy, $order)
            ->paginate($perPage);
    }

    /**
     * Get all question
     */
    public function getAllQuestion(string $orderBy = 'created_at', string $order = 'DESC', int $perPage = 4)
    {
        return $this->where('visibility', QuestionVisibility::OPEN)
            ->where('status', QuestionStatus::OPEN)
            ->where('group.group_id', null)
            ->orderBy($orderBy, $order)
            ->paginate($perPage);
    }

    /**
     * Get all question by field id
     */
    public function getAllQuestionByFieldId(int $fieldId, string $orderBy = 'created_at', string $order = 'DESC', int $perPage = 4)
    {
        return $this->where('visibility', QuestionVisibility::OPEN)
            ->where('field_id', $fieldId)
            ->where('status', QuestionStatus::OPEN)
            ->where('group.group_id', null)
            ->orderBy($orderBy, $order)
            ->paginate($perPage);
    }

    /**
     * Get all question of group by section id
     */
    public function getAllQuestionOfGroup(int $groupId, string $orderBy = 'created_at', string $order = 'DESC', int $perPage = 4)
    {
        return $this->where('group.group_id', $groupId)
            ->orderBy($orderBy, $order)
            ->paginate($perPage);
    }

    /**
     * Get all question of user in group
     */
    public function getAllQuestionOfUserInGroup(int $groupId, int $userId, string $orderBy = 'created_at', string $order = 'DESC', int $perPage = 4)
    {
        return $this->where('group.group_id', $groupId)
            ->where('user_id', $userId)
            ->orderBy($orderBy, $order)
            ->paginate($perPage);
    }

    /**
     * Get all question of group by section id
     */
    public function getAllQuestionOfGroupBySectionId(int $groupId, int $groupSectionId, string $orderBy = 'created_at', string $order = 'DESC', int $perPage = 4)
    {
        return $this->where('group.group_id', $groupId)
            ->where('group.group_section_id', $groupSectionId)
            ->orderBy($orderBy, $order)
            ->paginate($perPage);
    }

    /**
     * Update price for question
     */
    public function updatePrice(int $price, string $questionId)
    {
        $this->where('_id', new ObjectId($questionId))
            ->update(['price' => $price]);
    }

    /**
     * Change visibility of question
     */
    public function changeVisibility(int $visibility, string $questionId)
    {
        $this->where('_id', new ObjectId($questionId))
            ->update(['visibility' => $visibility]);
    }

    /**
     * Change status of question
     */
    public function changeStatus(int $status, string $questionId)
    {
        $this->where('_id', new ObjectId($questionId))
            ->update(['status' => $status]);
    }

    /**
     * Change field of question
     */
    public function changeField(int $fieldId, string $questionId)
    {
        $this->where('_id', new ObjectId($questionId))
            ->update(['field_id' => $fieldId]);
    }

    /**
     * Update tags of question
     */
    public function updateTags(array $tags, string $questionId)
    {
        $this->where('_id', new ObjectId($questionId))
            ->update(['tags' => $tags]);
    }

    /**
     * Update group section id
     */
    public function updateGroupSectionId(string $questionId, int $groupId, int $groupSectionId)
    {
        $this->where('_id', new ObjectId($questionId))
            ->where('group.group_id', $groupId)
            ->update(['group.group_section_id' => $groupSectionId]);
    }

    /**
     * Add comment for question
     * 
     * @param int $commentatorId
     * @param string $questionId
     * @param string $content
     */
    public function addComment(int $commentatorId, string $questionId, string $content)
    {
        $this->where('_id', new ObjectId($questionId))
            ->push(['comments' => [
                'commentator_id' => $commentatorId,
                'content' => $content,
                'time' => new UTCDateTime(),
            ]]);
    }

    /**
     * Update content of question
     * 
     * @param string $questionId
     * @param array $content
     */
    public function updateContentOfQuestion(string $questionId, array $content)
    {
        $this->where('_id', new ObjectId($questionId))
            ->update(['question_content' => $content]);
    }

    /**
     * Get all the field user questioned
     * 
     * @param int $userId
     */
    public function getAllFieldOfUser(int $userId)
    {
        return $this->where('user_id', $userId)
            ->pluck('field_id')
            ->toArray();
    }

    public function getTotalQuestionOfGroup(int $groupId)
    {
        return $this->where('group.group_id', $groupId)
            ->count();
    }

    /**
     * Get summary info of question for user
     * 
     * @param int $userId
     */
    public function getSummaryQuestionInfoOfUser(int $userId)
    {
        return $this->raw(function ($collection) use ($userId) {
            return $collection->aggregate([
                (object) [
                    '$match' => (object) [
                        'user_id' => $userId
                    ]
                ],
                (object) [
                    '$group' => (object) [
                        '_id' => '$status',
                        'count' => (object) [
                            '$sum' => 1
                        ]
                    ]
                ]
            ]);
        });
    }

    /**
     * Find question with multiple conditions
     */
    public function findQuestion($title, $content, $tags, $priceFrom, $priceTo, $userId, $fieldId, $orderBy = 'created_at', $order = 'desc', $perPage = 10)
    {
        $queryBuilder = $this;
        $this->where('question_content.content', (object) [
            '$regex' => 'doanh'
        ]);

        if (!is_null($title)) {
            $queryBuilder = $queryBuilder->where('question_content.title', (object) [
                '$regex' => $title
            ]);
        }

        if (!is_null($content)) {
            $queryBuilder = $queryBuilder->where('question_content.content', (object) [
                '$regex' => $content
            ]);
        }

        if (!is_null($tags)) {
            $queryBuilder = $queryBuilder->where('tags', $tags);
        }

        if (!is_null($priceFrom)) {
            $queryBuilder = $queryBuilder->where('price', '>=', $priceFrom);
        }

        if (!is_null($priceTo)) {
            $queryBuilder = $queryBuilder->where('price', '<=', $priceTo);
        }

        if (!is_null($userId)) {
            $queryBuilder = $queryBuilder->where('user_id', $userId);
        }

        if (!is_null($fieldId)) {
            $queryBuilder = $queryBuilder->where('field_id', $fieldId);
        }

        return $queryBuilder->orderBy($orderBy, $order)
            ->paginate($perPage);
    }
}
