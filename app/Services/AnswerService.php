<?php

namespace App\Services;

use App\Enums\AnswerStatus;
use App\Enums\QuestionStatus;
use App\Enums\QuestionVisibility;
use App\Enums\Role;
use App\Models\Answer;
use App\Models\Question;
use MongoDB\BSON\ObjectId;

class AnswerService
{
    protected $answer;
    protected $question;

    public function __construct(Answer $answer, Question $question)
    {
        $this->answer = $answer;
        $this->question = $question;
    }

    public function doesAnswerBelongToQuestion(string $answerId, string $questionId)
    {
        $answerInfo = $this->answer->getAnswerById($answerId);

        return $answerInfo->question_id == new ObjectId($questionId);
    }

    /**
     * Update status for answer
     */
    public function updateStatusAnswer(string $answerId, int $status, string $questionId)
    {
        $this->answer->updateStatus($answerId, $status);

        if ($status == AnswerStatus::ACCEPTED) {
            $this->question->changeStatus(QuestionStatus::RESOLVED, $questionId);
        }

        return $this->answer->getAnswerById($answerId)->user_id;
    }

    /**
     * Get all answer of a question without content
     * 
     * @param string $questionId
     */
    public function getAllAnswerOfQuestion(string $questionId)
    {
        return $this->answer->getAllAnswerOfQuestion($questionId);
    }

    /**
     * Get role of user with the answer
     * 
     * @param int $userId
     * @param string $answerId
     */
    public function getRoleOfUser(int $userId, string $answerId)
    {
        $answerInfo = $this->answer->getAnswerById($answerId);

        if ($userId == $answerInfo->user_id) {
            return Role::ANSWERER;
        }

        $questionInfo = $this->question->getQuestionInfo((string) $answerInfo->questionId);

        if ($userId == $questionInfo->user_id) {
            return Role::QUESTIONER;
        }

        return Role::VIEWER;
    }

    /**
     * Get detail of answer
     * 
     * @param int $userId
     * @param string $answerId
     */
    public function getAnswerDetail(int $userId, string $answerId)
    {
        $answerInfo = $this->answer->getAnswerById($answerId);

        if ($userId == $answerInfo->user_id) {
            return [
                'code' => 200,
                'data' => [
                    'answer' => $answerInfo,
                    'role' => Role::ANSWERER,
                ]
            ];
        }

        $questionInfo = $this->question->getQuestionInfo((string) $answerInfo->question_id);

        if ($userId == $questionInfo->user_id) {
            if ($answerInfo->status != AnswerStatus::WAITING) {
                return [
                    'code' => 200,
                    'data' => [
                        'answer' => $answerInfo,
                        'role' => Role::QUESTIONER
                    ]
                ];
            }

            if ($this->answer->canQuestionerView((string) $questionInfo->_id, $answerId)) {
                $this->answer->updateStatus($answerId, AnswerStatus::CONSIDERING);
                return [
                    'code' => 200,
                    'data' => [
                        'answer' => $answerInfo,
                        'role' => Role::QUESTIONER
                    ]
                ];
            } else {
                return [
                    'code' => 403,
                    'message' => trans('responses.questioner_can_not_view_answer'),
                    'data' => [
                        'role' => Role::QUESTIONER
                    ]
                ];
            };
        }

        if ($questionInfo->visibility == QuestionVisibility::OPEN) {
            if ($this->answer->canViewerView($answerId)) {
                return [
                    'code' => 200,
                    'data' => [
                        'role' => Role::VIEWER,
                        'answer' => $answerInfo
                    ]
                ];
            }
        }

        return [
            'code' => 403,
            'message' => trans('responses.viewer_can_not_view_answer'),
            'data' => [
                'role' => Role::VIEWER
            ]
        ];
    }

    /**
     * Get summary answer for user 
     * 
     * @param int $userId
     */
    public function getSummarayAnswerForUser(int $userId)
    {
        $summaryInfo = $this->answer->getSummaryAnswerOfUser($userId);
        $summaryInfo = $this->convertSummaryAnswerOfUser($summaryInfo);

        return [
            'waiting' => $summaryInfo[AnswerStatus::WAITING],
            'considering' => $summaryInfo[AnswerStatus::CONSIDERING],
            'accepted' => $summaryInfo[AnswerStatus::ACCEPTED],
            'rejected' => $summaryInfo[AnswerStatus::REJECTED]
        ];
    }

    /**
     * Convert data from array in array to an key value array
     * 
     * @param $summarayInfo
     */
    protected function convertSummaryAnswerOfUser($summarayInfo)
    {
        $result = [
            AnswerStatus::WAITING => 0,
            AnswerStatus::CONSIDERING => 0,
            AnswerStatus::ACCEPTED => 0,
            AnswerStatus::REJECTED => 0,
        ];

        foreach ($summarayInfo as $info) {
            $result[$info->_id] = $info->count;
        }

        return $result;
    }

    public function updateReportStatusForAnswer(string $answerId, int $reportStatus)
    {
        $this->answer->updateReportStatus($answerId, $reportStatus);
    }

    /**
     * Get all answer of user
     * 
     * @param int $userId
     */
    public function getAllAnswerOfUser(int $userId)
    {
        return $this->answer->getAllAnswerOfUser($userId)
            ->toArray();
    }
}
