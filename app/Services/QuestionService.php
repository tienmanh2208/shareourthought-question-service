<?php

namespace App\Services;

use App\Enums\AnswerStatus;
use App\Enums\QuestionStatus;
use App\Enums\QuestionVisibility;
use App\Models\Answer;
use App\Models\Question;
use MongoDB\BSON\ObjectId;

class QuestionService
{
    protected $answer;
    protected $question;

    public function __construct(Answer $answer, Question $question)
    {
        $this->answer = $answer;
        $this->question = $question;
    }

    /**
     * Check if question has considering answer or accepted answer
     */
    public function hasConsideringAnswer(string $questionId)
    {
        $listAnswers = $this->answer->getAnswersOfQuestion($questionId);
        foreach ($listAnswers as $answer) {
            if ($answer->status == AnswerStatus::CONSIDERING || $answer->status == AnswerStatus::ACCEPTED) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if an user is the author of the question
     * 
     * @return boolean
     */
    public function isTheAuthorOfQuestion(int $userId, string $questionId)
    {
        $questionInfo = $this->question->getQuestionInfo($questionId);

        if ($questionInfo->user_id == $userId) {
            return true;
        }

        return false;
    }

    /**
     * Check if info of question can be changed or not
     * Condition: user must be the author and question does not have any considering or accepted answer
     */
    public function canChangeInfoOfQuestion(int $userId, string $questionId)
    {
        if (
            $this->hasConsideringAnswer($questionId) ||
            !$this->isTheAuthorOfQuestion($userId, $questionId)
        ) {
            return false;
        };

        return true;
    }

    /**
     * Check if user can change price of question or not
     * Condition: If the question have no answer and user is the author of this question
     * 
     * @param int $userId id of user
     * @param string $questionId id of question
     */
    public function canChangePriceOfQuestion(int $userId, string $questionId)
    {
        if (
            $this->hasAnyAnswer($questionId) ||
            !$this->isTheAuthorOfQuestion($userId, $questionId)
        ) {
            return false;
        };

        return true;
    }

    /**
     * Check if a question has answer or not
     * 
     * @param string $questionId
     * 
     * @return boolean
     */
    public function hasAnyAnswer(string $questionId)
    {
        if (count($this->answer->getAnswersOfQuestion($questionId)) == 0) {
            return false;
        }

        return true;
    }

    /**
     * Check if question belongs to group or not
     * 
     * @param string $questionId
     * @param int $groupId
     * 
     * @return boolean
     */
    public function doesQuestionBelongToGroup(string $questionId, int $groupId)
    {
        $questionInfo = $this->question->getQuestionInfo($questionId);

        if (is_null($questionInfo)) {
            return false;
        }

        if ($questionInfo->group['group_id'] != $groupId) {
            return false;
        }

        return true;
    }

    /**
     * Update content for question
     * 
     * @param string $questionId
     * @param array $content
     */
    public function updateContentQuestion(string $questionId, array $content)
    {
        $this->question->updateContentOfQuestion($questionId, $content);
    }

    /**
     * Update group section id for question
     * 
     * @param string $questionId
     * @param int $groupId
     * @param int $groupSectionId
     */
    public function updateGroupIdForQuestion(string $questionId, int $groupId, int $groupSectionId)
    {
        $this->question->updateGroupSectionId($questionId, $groupId, $groupSectionId);
    }

    /**
     * Get detail of question
     * 
     * @param string $questionId
     * @param int $userId
     */
    public function getDetailOfQuestion(string $questionId, int $userId)
    {
        $questionInfo = $this->question->getQuestionInfo($questionId);

        if ($questionInfo->user_id === $userId || $questionInfo->visibility === QuestionVisibility::OPEN) {
            return $questionInfo;
        }

        return null;
    }

    /**
     * Get all the fields that are questioned by user
     * 
     * @param int $userId
     */
    public function getAllFieldOfUser(int $userId)
    {
        return array_values(array_unique($this->question->getAllFieldOfUser($userId)));
    }

    /**
     * Get question info of a group
     * 
     * @param int $groupId
     * @return array [
     *  total_questions => int
     * ]
     */
    public function getDetailOfGroup(int $groupId)
    {
        return [
            'total_questions' => $this->question->getTotalQuestionOfGroup($groupId),
        ];
    }

    public function getSummaryQuestionOfUser(int $userId)
    {
        $questionInfo = $this->question->getSummaryQuestionInfoOfUser($userId);

        $questionInfo = $this->convertSummaryQuestionOfUser($questionInfo);

        return [
            'open' => $questionInfo[QuestionStatus::OPEN],
            'resolved' => $questionInfo[QuestionStatus::RESOLVED],
            'cancel' => $questionInfo[QuestionStatus::CANCEL],
            'deleted' => $questionInfo[QuestionStatus::DELETED],
        ];
    }

    /**
     * Convert data from array in array to an key value array
     * 
     * @param $questionInfo
     */
    protected function convertSummaryQuestionOfUser($questionInfo)
    {
        $result = [
            QuestionStatus::OPEN => 0,
            QuestionStatus::RESOLVED => 0,
            QuestionStatus::CANCEL => 0,
            QuestionStatus::DELETED => 0,
        ];

        foreach ($questionInfo as $info) {
            $result[$info->_id] = $info->count;
        }

        return $result;
    }

    public function getListQuestionByListAnswerId(array $listAnswerId)
    {
        $listAnswer = array_values(array_diff(array_map(function ($answerId) {
            $answerInfo = $this->answer->getAnswerById($answerId);

            if (!is_null($answerInfo)) {
                return $answerInfo;
            }
        }, $listAnswerId), [null]));

        foreach ($listAnswer as &$answer) {
            $answer->question_info = $this->question->getQuestionInfo($answer->question_id);
        }

        return $listAnswer;
    }

    /**
     * Get list question by list id
     * 
     * @param array $listId
     */
    public function getListQuestionByListQuestionId(array $listQuestionId)
    {
        return array_values(array_diff(array_map(function (string $questionId) {
            $questionInfo = $this->question->getQuestionInfo($questionId);
            if (!is_null($questionInfo)) {
                return $questionInfo;
            }
        }, $listQuestionId), [null]));
    }

    public function findQuestion($title, $content, $tags, $priceFrom, $priceTo, $userId, $fieldId)
    {
        return $this->question->findQuestion($title, $content, $tags, $priceFrom, $priceTo, $userId, $fieldId);
    }
}
