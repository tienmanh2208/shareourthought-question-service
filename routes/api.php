<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'questions',
    'namespace' => 'Api\Questions',
], function () {
    Route::post('/create-question', 'CreateQuestionController@main');
    Route::post('/create-answer', 'CreateAnswerController@main');
    Route::post('/add-comment', 'AddCommentController@main');
    Route::post('/change-price', 'ChangePriceOfQuestion@main');
    Route::post('/change-visibility', 'ChangeVisibilityController@main');
    Route::post('/change-status', 'ChangeStatusController@main');
    Route::post('/change-field', 'ChangeFieldOfQuestionController@main');
    Route::post('/change-content', 'ChangeContentOfQuestionController@main');
    Route::post('/change-group-section', 'ChangeGroupSectionController@main');
    Route::post('/update-tags', 'UpdateTagsOfQuestionController@main');
    Route::get('/users/{userId}', 'GetListQuestionOfUserController@main');
    Route::post('/{questionId}/detail', 'GetDetailQuestionController@main');
    Route::post('/find-question', 'FindQuestionController@main');

    Route::group([
        'prefix' => 'users',
    ], function () {
        Route::get('/{userId}/fields', 'GetAllFieldOfUserController@main');
        Route::get('/{userId}/user-info', 'GetUserInfoController@main');
    });

    Route::get('/all', 'GetAllQuestionController@main');
    Route::get('/group/{groupId}/{groupSectionId}', 'GetListQuestionOfGroupController@main');
    Route::get('/group/{groupId}/user/{userId}', 'GetAllQuestionOfUserInGroupController@main');
    Route::get('/group/{groupId}/info/summary', 'GetInfoOfQuestionInGroupController@main');
});


Route::group([
    'prefix' => 'answers',
    'namespace' => 'Api\Answers',
], function () {
    Route::post('/update-status', 'UpdateStatusController@main');
    Route::post('/all/{questionId}', 'GetAllAnswerOfQuestionController@main');
    Route::post('/{answerId}/detail', 'GetDetailOfAnswerController@main');
    Route::post('/{answerId}/update-report-status', 'UpdateReportStatusController@main');
    Route::post('/list-question-by-answer-id', 'GetListQuestionByListAnswerIdController@main');
    Route::get('/all/{userId}', 'GetAllAnswerOfUserController@main');

    Route::post('/admin/update-answer-status', 'UpdateAnswerStatusAdminController@main');
});
