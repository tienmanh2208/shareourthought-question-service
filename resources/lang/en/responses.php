<?php

return [
    'create_question_successfully' => 'Create Question successfully',
    'request_denied' => 'Request is denied',
    'create_answer_successfully' => 'Create answer successfully',
    'update_question_price_successfully' => 'Update question price successfully',
    'update_question_visibility_successfully' => 'Update question visibility successfully',
    'update_question_status_successfully' => 'Update question status successfully',
    'update_question_field_successfully' => 'Update question field successfully',
    'update_question_tags_successfully' => 'Update question tags successfully',
    'update_question_content_successfully' => 'Update question content successfully',
    'update_question_section_id_successfully' => 'Update section id of question successfully',
    'add_comment_successfully' => 'Add comment successfully',

    'update_answer_status_successfully' => 'Update answer status successfully',
    'questioner_can_not_view_answer' => 'Không thể xem câu hỏi vì đang có câu trả lời cần xem xét hoặc câu hỏi đã được resolve',
    'viewer_can_not_view_answer' => 'Người xem chỉ có thể xem được câu trả lời người dùng đã chấp nhận',
    
    'have_no_permission' => 'You dont have permission',

    'answer' => [
        'update_report_status_successfully' => 'Update report status successfully',
    ],
];
